<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/** @var Laravel\Lumen\Routing\Router $router */
$router->get(
    '/',
    function () use ($router) {
        return $router->app->version();
    }
);
// | POST      | register        | App\Http\Controllers\RegisterController       |
$router->addRoute(['POST',], 'register', 'RegisterController');
// | POST      | login           | App\Http\Controllers\LoginController          |
$router->addRoute(['POST',], 'login', 'LoginController');

// | GET|HEAD  | companies         | App\Http\Controllers\CompanyController@index   |
$router->addRoute(['GET', 'HEAD',], 'companies', 'CompanyController@index');

$router->group(
    ['prefix' => 'states'],
    function () use ($router) {
        $router->get('/', 'StateController@index');
    }
);

$router->put('/refresh', 'ResetTokenController');

$router->group(
    ['middleware' => 'auth',],
    function () use ($router) {
        // | GET | verify | App\Http\Controllers\Auth\VerifyController |
        $router->addRoute(['GET', 'HEAD',], 'verify', 'VerifyController');
        // | GET | logout | App\Http\Controllers\Auth\LogoutController |
        $router->addRoute(['GET',], 'logout', 'LogoutController');

    }
);
