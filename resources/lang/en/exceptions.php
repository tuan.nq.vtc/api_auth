<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Exception Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in Exceptions thrown throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'roles' => [
        'already_exists' => 'That role already exists. Please choose a different name.',
        'cant_delete_admin' => 'You can not delete the Administrator role.',
        'create_failed' => 'There was a problem creating this role. Please try again.',
        'delete_failed' => 'There was a problem deleting this role. Please try again.',
        'has_users' => 'You can not delete a role with associated users.',
        'needs_permission' => 'You must select at least one permission for this role.',
        'not_found' => 'That role does not exist.',
        'update_failed' => 'There was a problem updating this role. Please try again.',
    ],
    'permissions' => [
        'already_exists' => 'That permission already exists. Please choose a different name.',
        'create_failed' => 'There was a problem creating this permission. Please try again.',
        'delete_failed' => 'There was a problem deleting this permission. Please try again.',
        'has_users' => 'You can not delete a permission with associated users.',
        'not_found' => 'That permission does not exist.',
        'update_failed' => 'There was a problem updating this permission. Please try again.',
    ],
    'users' => [
        'cant_deactivate_self' => 'You can not deactivate yourself.',
        'cant_delete_admin' => 'You can not delete the super administrator.',
        'cant_delete_self' => 'You can not delete yourself.',
        'cant_restore' => 'This user is not deleted so it can not be restored.',
        'company_need' => 'You must choose company',
        'create_failed' => 'There was a problem creating this user. Please try again.',
        'delete_failed' => 'There was a problem deleting this user. Please try again.',
        'delete_first' => 'This user must be deleted first before it can be destroyed permanently.',
        'email_change_not_allowed' => 'Email change is not allow',
        'email_failed' => 'That email address belongs to a different user.',
        'email_taken' => 'That e-mail address is already taken.',
        'mark_failed' => 'There was a problem updating this user. Please try again.',
        'not_found' => 'That user does not exist.',
        'old_password_not_matched' => 'Old password not matched',
        'profile_update_failed' => 'There was a problem updating your profile. Please try again',
        'register_failed' => 'There was a problem registering new account. Please try again.',
        'restore_failed' => 'There was a problem restoring this user. Please try again.',
        'role_needed_create' => 'You must choose at lease one role.',
        'role_needed' => 'You must choose at least one role.',
        'update_failed' => 'There was a problem updating this user. Please try again.',
        'update_password_failed' => 'There was a problem changing this user password. Please try again.',
    ],
    'company' => [
        'register_failed' => 'There was a problem registering new company. Please try again.',
    ],
    'auth' => [
        'login_failed' => 'Incorrect email or password.',
        'deactivated' => 'Your account has been deactivated.',
        'registration_disabled' => 'Registration is currently closed.',
    ],
];
