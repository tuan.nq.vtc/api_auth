<?php

use App\Models\Company;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Ramsey\Uuid\Uuid as PackageUuid;

/**
 * Class UserTableSeeder.
 */
class UserTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        $company = Company::firstWhere('name', 'Bamboo Metric LLC');
        // Add the master administrator, user id of 1
        $users = [
            [
                'id' => 1,
                'company_id' => $company->id,
                'uuid' => PackageUuid::uuid4()->toString(),
                'first_name' => 'Super',
                'last_name' => 'Admin',
                'email' => 'admin@admin.com',
                'password' => Hash::make('admin', []),
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed' => true,
            ],
            [
                'id' => 2,
                'company_id' => $company->id,
                'uuid' => PackageUuid::uuid4()->toString(),
                'first_name' => Faker\Factory::create()->firstName,
                'last_name' => Faker\Factory::create()->lastName,
                'email' => 'user@user.com',
                'password' => Hash::make('secret', []),
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed' => true,
            ],
            [
                'id' => 3,
                'company_id' => $company->id,
                'uuid' => PackageUuid::uuid4()->toString(),
                'first_name' => Faker\Factory::create()->firstName,
                'last_name' => Faker\Factory::create()->lastName,
                'email' => Faker\Factory::create()->email,
                'password' => Hash::make('secret', []),
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed' => true,
            ],
            [
                'id' => 4,
                'company_id' => $company->id,
                'uuid' => PackageUuid::uuid4()->toString(),
                'first_name' => Faker\Factory::create()->firstName,
                'last_name' => Faker\Factory::create()->lastName,
                'email' => Faker\Factory::create()->email,
                'password' => Hash::make('secret', []),
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed' => true,
            ],
            [
                'id' => 5,
                'company_id' => $company->id,
                'uuid' => PackageUuid::uuid4()->toString(),
                'first_name' => Faker\Factory::create()->firstName,
                'last_name' => Faker\Factory::create()->lastName,
                'email' => Faker\Factory::create()->email,
                'password' => Hash::make('secret', []),
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed' => true,
            ]
        ];

        User::insert($users);

        $this->enableForeignKeys();
    }
}
