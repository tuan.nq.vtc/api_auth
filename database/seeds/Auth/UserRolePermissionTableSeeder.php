<?php

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

/**
 * Class UserRoleTableSeeder.
 */
class UserRolePermissionTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        $adminPermissions = Permission::all();
        /** @var Role $adminRole */
        $adminRole = Role::findOrFail(1);
        $adminRole->givePermissionTo($adminPermissions);
        $userPermissions = $adminPermissions->filter(function ($permission) {
            return substr($permission->title, 0, 5) != 'user_'
                && substr($permission->name, 0, 5) != 'role_'
                && substr($permission->name, 0, 11) != 'permission_';
        });
        /** @var Role $userRole */
        $userRole = Role::findOrFail(2);
        $userRole->givePermissionTo($userPermissions);

        /** @var User $admin */
        $admin = User::findOrFail(1);
        $admin->assignRole($adminRole);
        /** @var User $user */
        $user = User::findOrFail(2);
        $user->assignRole($userRole);

        $this->enableForeignKeys();
    }
}
