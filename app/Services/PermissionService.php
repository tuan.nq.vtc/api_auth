<?php

namespace App\Services;

use App\Models\Permission;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class PermissionService
 * @package App\Services
 *
 * @property Permission|Builder $model
 */
class PermissionService extends BaseService
{
    protected static array $relations = [
        'roles',
    ];

    public function __construct(Permission $permission)
    {
        $this->model = $permission;
    }
}
