<?php

namespace App\Services;

use App\Models\Company;
use Illuminate\Database\Eloquent\{Builder, Model};

/**
 * Class CompanyService
 * @package App\Services
 *
 * @property Company|Builder $model
 */
class CompanyService
{
    public function __construct(Company $company)
    {
        $this->model = $company;
    }

    public function create(array $data): Model
    {
        return tap($this->model->newInstance($data), function ($instance) use ($data) {
            $instance->save();
        });
    }
}
