<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\{Builder, Collection, Model, ModelNotFoundException};
use Illuminate\Support\Str;

/**
 * Class BaseService
 * @package App\Services
 *
 * @property Model|Builder $model
 */
abstract class BaseService
{
    protected static array $relations = [];

    protected Model $model;

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->model->with(static::$relations)->get();
    }

    /**
     *
     * @param  array  $filters
     * @param  array  $sorts
     * @param  int  $page
     * @param  int  $limit
     *
     * @return LengthAwarePaginator
     */
    public function list(array $filters, array $sorts, int $page, int $limit): LengthAwarePaginator
    {
        $query = $this->model->with(static::$relations)->scopes($this->loadScopes($filters));
        foreach ($sorts as $column => $direction) {
            $query->orderBy($column, $direction);
        }

        return $query->paginate($limit, ['*'], config('pagination.page_name'), $page);
    }

    /**
     * @param  int  $id
     *
     * @return Model
     *
     * @throws ModelNotFoundException
     */
    public function get($id): Model
    {
        return $this->model->findOrFail($id)->load(static::$relations);
    }

    /**
     * @param  array  $data
     *
     * @return Model
     *
     * @throws GeneralException
     */
    public function create(array $data): Model
    {
        return tap(
            $this->model->create($data),
            function ($instance) {
                if (!$instance) {
                    $key = "exceptions.{$this->model->getTable()}.create_failed";
                    throw new GeneralException(__($key));
                }
            }
        );
    }

    /**
     * @param $id
     * @param  array  $data
     *
     * @return Model
     *
     * @throws ModelNotFoundException|GeneralException
     */
    public function update($id, array $data): Model
    {
        $model = tap(
            $this->model->findOrFail($id),
            function (Model $instance) use ($data) {
                if (!$instance->update($data)) {
                    $key = "exceptions.{$this->model->getTable()}.update_failed";
                    throw new GeneralException(__($key));
                }
            }
        );

        return $model->load(static::$relations);
    }

    /**
     * @param $id
     *
     * @throws ModelNotFoundException|GeneralException
     */
    public function delete($id): void
    {
        $model = $this->model->findOrFail($id);
        try {
            $model->delete();
        } catch (Exception $e) {
            $key = "exceptions.{$this->model->getTable()}.delete_failed";
            throw new GeneralException(__($key));
        }
    }

    private function loadScopes($scopes): array
    {
        $namedScoped = [];
        foreach ($scopes as $name => $args) {
            $scopeName = Str::camel($name);
            if (!$this->model->hasNamedScope($scopeName)) {
                continue;
            }
            $namedScoped[$scopeName] = $args;
        }

        return $namedScoped;
    }
}
