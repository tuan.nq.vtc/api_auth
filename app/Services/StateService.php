<?php

namespace App\Services;

use App\Models\State;

/**
 * Class LicenseService
 * @package App\Services
 *
 */
class StateService
{
    public function all()
    {
        return State::all(['code', 'name']);
    }
}
