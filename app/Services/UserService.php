<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Models\User;
use Illuminate\Database\Eloquent\{Builder, Model};

/**
 * Class UserService
 * @package App\Services
 *
 * @property User|Builder $model
 */
class UserService extends BaseService
{

    protected static array $relations = [
        'roles',
        'roles.permissions',
        'permissions',
    ];

    public function __construct(User $user)
    {
        $this->model = $user;
    }

    /**
     * @param array $data
     * @return Model
     * @throws GeneralException
     */
    public function create(array $data): Model
    {
        try {
            return tap($this->model->newInstance($data), function ($instance) use ($data) {
                $this->syncRelations($instance, $data);
                $instance->save();
            });
        } catch (\Exception $e) {
            throw new GeneralException(__("exceptions.users.create_failed"));
        }
    }

    /**
     * @param $user
     * @param array $data
     */
    protected function syncRelations($user, array $data)
    {
        $filter = function ($value) {
            return !is_null($value) && $value !== '';
        };

        if (!empty($data['roles'])) {
            $roles = array_filter($data['roles'], $filter);
            $user->syncRoles($roles);
        }

        if (!empty($data['permissions'])) {
            $permissions = array_filter($data['permissions'], $filter);
            $user->syncPermissions($permissions ?? []);
        }

        if (!empty($data['licenses'])) {
            $licenses = array_filter($data['licenses'], $filter);
            $user->syncLicenses($licenses);
        }
    }
}
