<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Models\{User};
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\{JWTAuth, JWTFactory};
use Tymon\JWTAuth\Payload;
use Tymon\JWTAuth\Token;

class AuthService
{

    /**
     * @return string | \Exception|\Throwable
     */
    public function makeRefreshToken()
    {
        try {
            $payload = JWTFactory::customClaims(
                [
                    'sub' => auth()->user()->username,
                    'exp' => Carbon::now()->addWeeks(config('auth.week_overdue_period'))->timestamp
                ]
            )
                ->aud('secret')->secret(['user_id' => auth()->user()->id])
                ->make();
            return JWTAuth::encode($payload)->get();
        } catch (\Throwable $throwable) {
            return new GeneralException("Has some errors while make token");
        }
    }

    /**
     * @param string $jwtToken
     *
     * @return string |\Exception
     */
    public function refreshToken(string $jwtToken)
    {
        try {
            $token = new Token($jwtToken);
            /** @var Payload $payload */
            $payload = JWTAuth::decode($token);
            if (!is_null($secret = $payload->get('secret'))) {
                $user = User::find($secret->user_id);
                return JWTAuth::fromUser($user);
            }
            return Auth::guard()->refresh();
        } catch (\Exception $e) {
            return $e;
        }
    }
}
