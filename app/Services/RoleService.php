<?php

namespace App\Services;

use App\Models\Role;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserService
 * @package App\Services
 *
 * @property Role|Builder $model
 */
class RoleService
{
    /**
     * @param $company
     * @return array
     */
    public function createByCompany($company)
    {
        $companyAdminRole = Role::create(
            [
                'name' => $this->formatName("{$company->name} Admin"),
                'label' => Role::LABEL_ADMIN,
                'company_id' => $company->id
            ]
        );
        $adminRole = Role::findByName(config('access.roles.admin_role'));
        $this->handlePermission($companyAdminRole, $adminRole->permissions);
        Role::create(
            [
                'name' => $this->formatName("{$company->name} User"),
                'label' => Role::LABEL_USER,
                'company_id' => $company->id
            ]
        );

        return [$adminRole->name, $companyAdminRole->name];
    }

    /**
     * @param string $name
     * @return string
     */
    private function formatName(string $name)
    {
        return strtolower(str_replace(' ', '_', $name));
    }

    /**
     * #todo: permissions should be limited by modules of company and parent role.
     * @param Role $role
     * @param $permissions
     */
    private function handlePermission(Role $role, $permissions)
    {
        $role->syncPermissions($permissions);
    }
}
