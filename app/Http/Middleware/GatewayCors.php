<?php namespace App\Http\Middleware;

use Closure;

class GatewayCors
{
    /**
     * @param $request
     * @param Closure $next
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle($request, Closure $next)
    {
        if ($request->header('X-Forwarded-Method') === 'OPTIONS') {
            return response()->json(null, 200);
        }

        return $next($request);
    }
}
