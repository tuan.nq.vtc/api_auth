<?php

namespace App\Http\Controllers;

use App\Exceptions\GeneralException;
use App\Http\Controllers\Traits\ReponseWithToken;
use App\Models\Company;
use App\Services\AuthService;
use App\Services\CompanyService;
use App\Services\RoleService;
use App\Services\UserService;
use Carbon\Carbon;
use Illuminate\Http\{JsonResponse, Request, Response};
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\Controller;

class RegisterController extends Controller
{
    use ReponseWithToken;

    /**
     * @param Request        $request
     * @param CompanyService $companyService
     * @param UserService    $userService
     * @param RoleService    $roleService
     * @param AuthService    $authService
     *
     * @return JsonResponse
     *
     * @throws ValidationException
     */
    public function __invoke(
        Request $request,
        CompanyService $companyService,
        UserService $userService,
        RoleService $roleService,
        AuthService $authService
    ) {
        $rules = [
            'company.name' => ['required', 'string', 'max:100', 'unique:App\Models\Company,name'],
            'company.address1' => ['required', 'string', 'max:100'],
            'company.address2' => ['nullable', 'string', 'max:100'],
            'company.city' => ['required', 'string'],
            'company.state_code' => ['required', 'string'],
            'company.zip_code' => ['required', 'string'],
            'company.ein' => ['required', 'string', 'max:9', 'unique:App\Models\Company,ein'],
            'company.employee_range' => ['nullable', Rule::in(array_keys(Company::EMPLOYEE_RANGES))],
            'user.first_name' => ['required', 'string', 'max:100'],
            'user.last_name' => ['required', 'string', 'max:100'],
            'user.phone' => ['required', 'string', 'max:20'],
            'user.email' => ['required', 'email', 'unique:App\Models\User,email'],
            'user.password' => ['required', 'confirmed', 'min:8'],
        ];
        $customAttributes = [
            'company.name' => 'company name',
            'company.address1' => 'company address line 1',
            'company.address2' => 'company address line 2',
            'company.city' => 'company city',
            'company.state_code' => 'company state',
            'company.zip_code' => 'company ZIP code',
            'company.ein' => 'company EIN',
            'company.employee_range' => 'company employee range',
            'user.first_name' => 'user first name',
            'user.last_name' => 'user last name',
            'user.phone' => 'user phone',
            'user.email' => 'user email',
            'user.password' => 'user password',
        ];
        $data = $this->validate($request, $rules, [], $customAttributes);
        try {
            DB::beginTransaction();

            if (!($company = $companyService->create($data['company']))) {
                new GeneralException(__('exceptions.company.register_failed'));
            }
            $roles = $roleService->createByCompany($company);
            if (!($user = $userService->create(array_merge($data['user'], [
                'company_id' => $company->id,
                'roles' => $roles
            ])))) {
                new GeneralException(__('exceptions.users.register_failed'));
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
        if (!$token = auth()->login($user)) {
            return response()->json(['message' => __('exceptions.auth.login_failed')], Response::HTTP_UNAUTHORIZED);
        }
        $this->authenticated($request, auth()->user());
        return $this->respondWithToken($token, $authService->makeRefreshToken());
    }

    /**
     * @param Request $request
     * @param $user
     */
    protected function authenticated(Request $request, $user)
    {
        $user->update(
            [
                'last_login_at' => Carbon::now()->toDateTimeString(),
                'last_login_ip' => $request->getClientIp()
            ]
        );
    }

}
