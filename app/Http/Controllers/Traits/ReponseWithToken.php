<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

trait ReponseWithToken
{
    /**
     * @param string $token
     * @param string $refreshToken
     * @param int    $status
     *
     * @return JsonResponse
     */
    protected function respondWithToken(string $token, $refreshToken = '', $status = Response::HTTP_OK)
    {
        return response()->json(
            [
                'token' => $token,
                'token_type' => 'bearer',
                'refresh_token' => $refreshToken
            ],
            $status
        );
    }
}
