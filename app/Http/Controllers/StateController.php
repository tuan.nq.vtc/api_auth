<?php

namespace App\Http\Controllers;

use App\Services\StateService;
use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Routing\Controller;

/**
 * Class StateController
 * @package App\Http\Controllers
 */
class StateController extends Controller
{
    /**
     * @var StateService
     */
    private StateService $stateService;

    /**
     * StateController constructor.
     * @param StateService $stateService
     */
    public function __construct(StateService $stateService)
    {
        $this->stateService = $stateService;
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        return response()->json($this->stateService->all());
    }
}
