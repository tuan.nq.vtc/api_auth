<?php

namespace App\Http\Controllers;

use Illuminate\Http\{JsonResponse, Request, Response};
use Laravel\Lumen\Routing\Controller;

/**
 * Class VerifyController
 * @package App\Http\Controllers\Auth
 */
class VerifyController extends Controller
{
    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        if ($request->header('X-Forwarded-Method') === 'OPTIONS') {
            return response()->json(null, Response::HTTP_OK);
        }
        return response()->json(
            $request->user(),
            Response::HTTP_OK,
            [config('app.user_forward.headers.auth_user_uuid') => $request->user()->id]
        );
    }
}
