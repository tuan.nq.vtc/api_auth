<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\ReponseWithToken;
use App\Services\AuthService;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\Controller;
use Tymon\JWTAuth\Facades\{JWTAuth, JWTFactory};

class LoginController extends Controller
{
    use ReponseWithToken;

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function __invoke(Request $request, AuthService $authService)
    {
        $credentials = $this->validate(
            $request,
            [
                'email' => 'required|email',
                'password' => 'required|string',
            ]
        );
        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['message' => __('exceptions.auth.login_failed')], Response::HTTP_UNAUTHORIZED);
        }
        $this->authenticated($request, auth()->user());
        return $this->respondWithToken($token, $authService->makeRefreshToken());
    }

    protected function authenticated(Request $request, $user)
    {
        $user->update(
            [
                'last_login_at' => Carbon::now()->toDateTimeString(),
                'last_login_ip' => $request->getClientIp()
            ]
        );
    }

}
