<?php

namespace App\Http\Controllers;

use App\Exceptions\GeneralException;
use App\Http\Controllers\Traits\ReponseWithToken;
use App\Services\AuthService;
use Illuminate\Http\{Request, Response};
use Laravel\Lumen\Routing\Controller;

class ResetTokenController extends Controller
{

    use ReponseWithToken;

    public function __invoke(Request $request, AuthService $authService)
    {
        $refreshToken = $request->get('refresh_token', '');
        if ($refreshToken == '') {
            throw new GeneralException('Missing refresh token parameter');
        }
        if (is_string($token = $authService->refreshToken($refreshToken))) {
            return $this->respondWithToken($token, $refreshToken, Response::HTTP_OK);
        }
        return response('Unauthorized', Response::HTTP_UNAUTHORIZED);
    }
}
