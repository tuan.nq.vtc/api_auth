<?php

namespace App\Http\Controllers;

use App\Services\CompanyService;
use Illuminate\Http\JsonResponse;

/**
 * Class CompanyController
 * @package App\Http\Controllers
 */
class CompanyController
{
    /**
     * @var CompanyService
     */
    private CompanyService $companyService;

    public function __construct(CompanyService $companyService)
    {
        $this->companyService = $companyService;
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        return response()->json($this->companyService->getAvailableItems());
    }
}
