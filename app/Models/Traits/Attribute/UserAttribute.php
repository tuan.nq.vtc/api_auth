<?php

namespace App\Models\Traits\Attribute;

use Illuminate\Support\Facades\Hash;

/**
 * Trait UserAttribute.
 */
trait UserAttribute
{

    public function getEmailChangedAttribute(): bool
    {
        return $this->wasChanged('email');
    }

    public function getFullNameAttribute(): string
    {
        return $this->last_name
            ? $this->first_name.' '.$this->last_name
            : $this->first_name;
    }

    public function getNameAttribute(): string
    {
        return $this->full_name;
    }

    public function getPermissionListAttribute():array
    {
        return $this->getPermissionNames()->toArray();
    }

    public function getPictureAttribute()
    {
        return $this->getPicture();
    }

    public function getRolesLabelAttribute(): string
    {
        $roles = $this->getRoleNames()->toArray();

        if (\count($roles)) {
            return implode(
                ', ',
                array_map(
                    function ($item) {
                        return ucwords($item);
                    },
                    $roles
                )
            );
        }

        return 'N/A';
    }

    public function getRoleListAttribute(): array
    {
        return $this->getRoleNames()->toArray();
    }

    public function getPermissionsLabelAttribute(): string
    {
        $permissions = $this->getDirectPermissions()->toArray();

        if (\count($permissions)) {
            return implode(
                ', ',
                array_map(
                    function ($item) {
                        return ucwords($item['name']);
                    },
                    $permissions
                )
            );
        }

        return 'N/A';
    }

    public function setPasswordAttribute(string $password): void
    {
        // If password was accidentally passed in already hashed, try not to double hash it
        if (
            (\strlen($password) === 60 && preg_match('/^\$2y\$/', $password)) ||
            (\strlen($password) === 95 && preg_match('/^\$argon2i\$/', $password))
        ) {
            $hash = $password;
        } else {
            $hash = Hash::make($password);
        }

        // Note: Password Histories are logged from the \App\Observer\User\UserObserver class
        $this->attributes['password'] = $hash;
    }
}
