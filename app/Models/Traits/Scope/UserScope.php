<?php

namespace App\Models\Traits\Scope;

use Illuminate\Database\Eloquent\Builder;

/**
 * Class UserScope.
 */
trait UserScope
{

    /**
     * Scope a query to active/non-active users
     *
     * @param $query
     * @param  bool  $status
     *
     * @return Builder
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('active', $status);
    }

    /**
     * Scope a query to users confirmation status
     *
     * @param $query
     * @param  bool  $confirmed
     *
     * @return Builder
     */
    public function scopeConfirmed($query, $confirmed = true)
    {
        return $query->where('confirmed', $confirmed);
    }

    /**
     * Scope a query to user emails contain email
     *
     * @param $query
     * @param $email
     *
     * @return Builder
     */
    public function scopeEmail($query, $email)
    {
        return $query->where('email', 'LIKE', $email);
    }

    /**
     * Scope a query to users first name and last name
     *
     * @param $query
     * @param  bool  $name
     *
     * @return Builder
     */
    public function scopeName($query, $name = true)
    {
        return $query->where(function ($query) use ($name) {
            $query->orWhere('first_name', 'LIKE', "%{$name}%")
                ->orWhere('last_name', 'LIKE', "%{$name}%");
        });
    }

    /**
     * Scope a query to users first name
     *
     * @param $query
     * @param  bool  $name
     *
     * @return Builder
     */
    public function scopeFirstName($query, $name = true)
    {
        return $query->where('first_name', 'LIKE', "%{$name}%");
    }

    /**
     * Scope a query to users first name
     *
     * @param $query
     * @param  bool  $name
     *
     * @return Builder
     */
    public function scopeLastName($query, $name = true)
    {
        return $query->where('last_name', 'LIKE', "%{$name}%");
    }
}
