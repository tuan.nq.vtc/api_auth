<?php

namespace App\Models\Traits\Scope;

use Illuminate\Database\Eloquent\Builder;

trait NameFilterable
{
    /**
     * Scope a query to records contain name.
     *
     * @param  Builder  $query
     * @param  string  $name
     *
     * @return Builder
     */
    public function scopeName($query, $name)
    {
        return $query->where('name', 'LIKE', "%{$name}%");
    }
}
