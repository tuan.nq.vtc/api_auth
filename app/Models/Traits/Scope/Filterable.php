<?php

namespace App\Models\Traits\Scope;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

trait Filterable
{
    /**
     * @param Builder $query
     * @param $parameters
     *
     * @return Builder
     */
    public function scopeFilter($query, $parameters)
    {
        if (\is_array($parameters)) {
            foreach ($parameters as $field => $value) {
                $method = 'scope' . Str::studly($field);
                if (method_exists($this, $method)) {
                    $this->{$method}($query, $value);
                }
            }
        }

        return $query;
    }
}
