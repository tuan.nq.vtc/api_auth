<?php

namespace App\Models\Traits;

use Ramsey\Uuid\Uuid;

/**
 * Trait UsesUuid.
 */
trait UsesUuid
{

    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }

    /**
     * The "booted" method of the model.
     */
    protected static function booted(): void
    {
        static::creating(
            function (self $model): void {
                // Automatically generate a UUID if using them, and not provided.
                if (empty($model->{$model->getKeyName()})) {
                    $model->setAttribute($model->getKeyName(), $model->generateUuid());
                }
            }
        );
    }

    /**
     * Get a new version 4 (random) UUID.
     */
    public function generateUuid()
    {
        return Uuid::uuid4()->toString();
    }
}
