<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sushi\Sushi;

/**
 * Class State
 * @package App\Models
 *
 * @property-read string $code
 * @property-read string $name
 * @property-read string $capital
 * @property-read string $regulator
 */
class State extends Model
{
    use Sushi;

    const REGULATOR_LEAF = 'leaf';
    const REGULATOR_METRC = 'metrc';

    protected $rows = [
        [
            'code' => 'AL',
            'name' => 'Alabama',
            'capital' => 'Montgomery',
            'regulator' => ''
        ],
        [
            'code' => 'AK',
            'name' => 'Alaska',
            'capital' => 'Juneau',
            'regulator' => self::REGULATOR_METRC
        ],
        [
            'code' => 'AZ',
            'name' => 'Arizona',
            'capital' => 'Phoenix',
            'regulator' => ''
        ],
        [
            'code' => 'AR',
            'name' => 'Arkansas',
            'capital' => 'Little Rock',
            'regulator' => ''
        ],
        [
            'code' => 'CA',
            'name' => 'California',
            'capital' => 'Sacramento',
            'regulator' => self::REGULATOR_METRC,
        ],
        [
            'code' => 'CO',
            'name' => 'Colorado',
            'capital' => 'Denver',
            'regulator' => self::REGULATOR_METRC,
        ],
        [
            'code' => 'CT',
            'name' => 'Connecticut',
            'capital' => 'Hartford',
            'regulator' => ''
        ],
        [
            'code' => 'DE',
            'name' => 'Delaware',
            'capital' => 'Dover',
            'regulator' => ''
        ],
        [
            'code' => 'FL',
            'name' => 'Florida',
            'capital' => 'Tallahassee',
            'regulator' => ''
        ],
        [
            'code' => 'GA',
            'name' => 'Georgia',
            'capital' => 'Atlanta',
            'regulator' => ''
        ],
        [
            'code' => 'HI',
            'name' => 'Hawaii',
            'capital' => 'Honolulu',
            'regulator' => ''
        ],
        [
            'code' => 'ID',
            'name' => 'Idaho',
            'capital' => 'Boise',
            'regulator' => ''
        ],
        [
            'code' => 'IL',
            'name' => 'Illinois',
            'capital' => 'Springfield',
            'regulator' => ''
        ],
        [
            'code' => 'IN',
            'name' => 'Indiana',
            'capital' => 'Indianapolis',
            'regulator' => ''
        ],
        [
            'code' => 'IA',
            'name' => 'Iowa',
            'capital' => 'Des Moines',
            'regulator' => ''
        ],
        [
            'code' => 'KS',
            'name' => 'Kansas',
            'capital' => 'Topeka',
            'regulator' => ''
        ],
        [
            'code' => 'KY',
            'name' => 'Kentucky',
            'capital' => 'Frankfort',
            'regulator' => ''
        ],
        [
            'code' => 'LA',
            'name' => 'Louisiana',
            'capital' => 'Baton Rouge',
            'regulator' => self::REGULATOR_METRC
        ],
        [
            'code' => 'ME',
            'name' => 'Maine',
            'capital' => 'Augusta',
            'regulator' => self::REGULATOR_METRC
        ],
        [
            'code' => 'MD',
            'name' => 'Maryland',
            'capital' => 'Annapolis',
            'regulator' => self::REGULATOR_METRC
        ],
        [
            'code' => 'MA',
            'name' => 'Massachusetts',
            'capital' => 'Boston',
            'regulator' => self::REGULATOR_METRC,
        ],
        [
            'code' => 'MI',
            'name' => 'Michigan',
            'capital' => 'Lansing',
            'regulator' => self::REGULATOR_METRC
        ],
        [
            'code' => 'MN',
            'name' => 'Minnesota',
            'capital' => 'St. Paul',
            'regulator' => ''
        ],
        [
            'code' => 'MS',
            'name' => 'Mississippi',
            'capital' => 'Jackson',
            'regulator' => ''
        ],
        [
            'code' => 'MO',
            'name' => 'Missouri',
            'capital' => 'Jefferson City',
            'regulator' => self::REGULATOR_METRC
        ],
        [
            'code' => 'MT',
            'name' => 'Montana',
            'capital' => 'Helena',
            'regulator' => self::REGULATOR_METRC
        ],
        [
            'code' => 'NE',
            'name' => 'Nebraska',
            'capital' => 'Lincoln',
            'regulator' => ''
        ],
        [
            'code' => 'NV',
            'name' => 'Nevada',
            'capital' => 'Carson City',
            'regulator' => self::REGULATOR_METRC
        ],
        [
            'code' => 'NH',
            'name' => 'New Hampshire',
            'capital' => 'Concord',
            'regulator' => ''
        ],
        [
            'code' => 'NJ',
            'name' => 'New Jersey',
            'capital' => 'Trenton',
            'regulator' => ''
        ],
        [
            'code' => 'NM',
            'name' => 'New Mexico',
            'capital' => 'Santa Fe',
            'regulator' => ''
        ],
        [
            'code' => 'NY',
            'name' => 'New York',
            'capital' => 'Albany',
            'regulator' => ''
        ],
        [
            'code' => 'NC',
            'name' => 'North Carolina',
            'capital' => 'Raleigh',
            'regulator' => ''
        ],
        [
            'code' => 'ND',
            'name' => 'North Dakota',
            'capital' => 'Bismarck',
            'regulator' => ''
        ],
        [
            'code' => 'OH',
            'name' => 'Ohio',
            'capital' => 'Columbus',
            'regulator' => self::REGULATOR_METRC
        ],
        [
            'code' => 'OK',
            'name' => 'Oklahoma',
            'capital' => 'Oklahoma City',
            'regulator' => ''
        ],
        [
            'code' => 'OR',
            'name' => 'Oregon',
            'capital' => 'Salem',
            'regulator' => self::REGULATOR_METRC
        ],
        [
            'code' => 'PA',
            'name' => 'Pennsylvania',
            'capital' => 'Harrisburg',
            'regulator' => ''
        ],
        [
            'code' => 'RI',
            'name' => 'Rhode Island',
            'capital' => 'Providence',
            'regulator' => ''
        ],
        [
            'code' => 'SC',
            'name' => 'South Carolina',
            'capital' => 'Columbia',
            'regulator' => ''
        ],
        [
            'code' => 'SD',
            'name' => 'South Dakota',
            'capital' => 'Pierre',
            'regulator' => ''
        ],
        [
            'code' => 'TN',
            'name' => 'Tennessee',
            'capital' => 'Nashville',
            'regulator' => ''
        ],
        [
            'code' => 'TX',
            'name' => 'Texas',
            'capital' => 'Austin',
            'regulator' => ''
        ],
        [
            'code' => 'UT',
            'name' => 'Utah',
            'capital' => 'Salt Lake City',
            'regulator' => ''
        ],
        [
            'code' => 'VT',
            'name' => 'Vermont',
            'capital' => 'Montpelier',
            'regulator' => ''
        ],
        [
            'code' => 'VA',
            'name' => 'Virginia',
            'capital' => 'Richmond',
            'regulator' => ''
        ],
        [
            'code' => 'WA',
            'name' => 'Washington',
            'capital' => 'Olympia',
            'regulator' => self::REGULATOR_LEAF,
        ],
        [
            'code' => 'WV',
            'name' => 'West Virginia',
            'capital' => 'Charleston',
            'regulator' => ''
        ],
        [
            'code' => 'WI',
            'name' => 'Wisconsin',
            'capital' => 'Madison',
            'regulator' => ''
        ],
        [
            'code' => 'WY',
            'name' => 'Wyoming',
            'capital' => 'Cheyenne',
            'regulator' => ''
        ]
    ];
}
