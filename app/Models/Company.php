<?php

namespace App\Models;

use App\Models\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Company
 * @package App\Models
 *
 * @property int $id
 * @property string $uuid
 * @property int $name
 * @property string $address1
 * @property string $address2
 *
 * @property-read string address
 */
class Company extends Model
{
    use UsesUuid;

    const EMPLOYEE_RANGES = [
        '1-20' => ['min' => 1, 'max' => 20],
        '21-200' => ['min' => 21, 'max' => 200],
        '201-10000' => ['min' => 201, 'max' => 10000],
        '10001+' => ['min' => 10001, 'max' => null],
    ];

    protected $fillable = [
        'name',
        'logo',
        'address1',
        'address2',
        'city',
        'state_code',
        'zip_code',
        'employee_range',
        'ein',
        'timezone',
        'status',
    ];

    public function getAddressAttribute()
    {
        return $this->attributes['address1'] . ' ' . $this->attributes['address2'];
    }
}
