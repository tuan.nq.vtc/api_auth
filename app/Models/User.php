<?php

namespace App\Models;

use Altek\Accountant\{Contracts\Recordable as RecordableContract, Recordable};
use Altek\Eventually\Eventually;
use App\Models\Traits\Attribute\UserAttribute;
use App\Models\Traits\HasToken;
use App\Models\Traits\Scope\Filterable;
use App\Models\Traits\Scope\UserScope;
use App\Models\Traits\UsesUuid;
use DateTime;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\{Access\Authorizable as AuthorizableContract, Authenticatable as AuthenticatableContract};
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Lumen\Auth\Authorizable;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject as JwtAuthContract;

/**
 * Class User
 * @package App\Models
 *
 * @property int id
 * @property int company_id
 * @property int license_id
 * @property string first_name
 * @property string last_name
 * @property-read string full_name
 * @property string email
 * @property string avatar_type
 * @property string avatar_location
 * @property-read  string picture
 * @property string password
 * @property DateTime|null password_changed_at
 * @property bool active
 * @property string|null confirmation_code
 * @property bool confirmed
 * @property DateTime|null last_login_at
 * @property string|null last_login_ip
 * @property-read string role_label
 * @property-read string permission_label
 * @property-read bool|null email_changed
 * @property bool can_change_password
 * @property bool can_update_profile
 * @property bool can_upload_photo
 * @property DateTime|null created_at
 * @property DateTime|null updated_at
 * @property DateTime|null deleted_at
 *
 * @property-read \App\Models\Company company
 */
class User extends Model implements AuthenticatableContract, AuthorizableContract, RecordableContract,
                                        JwtAuthContract
{
    use UsesUuid;
    use Authenticatable;
    use Authorizable;
    use Eventually;
    use HasRoles;
    use HasToken;
    use Recordable;
    use SoftDeletes;

    use Filterable;
    use UserAttribute;
    use UserScope;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'company_id',
        'first_name',
        'last_name',
        'email',
        'phone',
        'avatar_type',
        'avatar_location',
        'password',
        'password_changed_at',
        'active',
        'confirmation_code',
        'confirmed',
        'last_login_at',
        'last_login_ip',
        'can_change_password',
        'can_update_profile',
        'can_upload_photo',
    ];

    /**
     * The dynamic attributes from mutators that should be returned with the user object.
     * @var array
     */
    protected $appends = [
        'full_name',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
        'confirmed' => 'boolean',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'last_login_at',
        'password_changed_at',
    ];
}
