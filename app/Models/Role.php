<?php

namespace App\Models;

use App\Models\Traits\Scope\Filterable;
use App\Models\Traits\Scope\RoleScope;
use App\Models\Traits\UsesUuid;
use DateTime;
use Spatie\Permission\Models\Role as BaseRole;

/**
 * Class Role
 * @package App\Models
 *
 * @property int id
 * @property string name
 * @property string guard
 * @property DateTime created_at
 * @property DateTime deleted_at
 */
class Role extends BaseRole
{
    use UsesUuid;
    use Filterable;
    use RoleScope;

    public const LABEL_ADMIN = 'Admin';
    public const LABEL_USER = 'User';

    public $incrementing = false;

    public $keyType = 'string';

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'pivot'
    ];

    protected $fillable = [
        'company_id',
        'name',
        'label',
        'guard_name',
        'description',
    ];
}
