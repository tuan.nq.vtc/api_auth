<?php

namespace App\Models;

use App\Models\Traits\Scope\Filterable;
use App\Models\Traits\Scope\PermissionScope;
use App\Models\Traits\UsesUuid;
use DateTime;
use Spatie\Permission\Models\Permission as BasePermission;

/**
 * Class Permission
 * @package App\Models
 *
 * @property int id
 * @property string name
 * @property string guard
 * @property DateTime created_at
 * @property DateTime deleted_at
 */
class Permission extends BasePermission
{
    use UsesUuid;
    use Filterable;
    use PermissionScope;

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'pivot'
    ];
}
