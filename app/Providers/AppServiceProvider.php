<?php

namespace App\Providers;

use Altek\Accountant\AccountantServiceProvider;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Illuminate\Cache\CacheManager;
use Illuminate\Redis\RedisServiceProvider;
use Illuminate\Support\ServiceProvider;
use Spatie\Permission\PermissionServiceProvider;
use Tymon\JWTAuth\Providers\LumenServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RedisServiceProvider::class);
        $this->app->alias('cache', CacheManager::class);

        if ($this->app->environment() !== 'production') {
            $this->app->register(IdeHelperServiceProvider::class);
        }

        $this->app->register(AccountantServiceProvider::class);
        $this->app->register(LumenGravatarServiceProvider::class);
        $this->app->register(PermissionServiceProvider::class);
        $this->app->register(LumenServiceProvider::class);
    }
}
