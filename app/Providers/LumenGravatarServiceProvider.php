<?php

namespace App\Providers;

use Creativeorange\Gravatar\Gravatar;
use Illuminate\Support\ServiceProvider;

class LumenGravatarServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    public function boot()
    {
        $this->app->singleton('gravatar', function () {
            return new Gravatar;
        });
    }
}
