FROM composer:1.10 as composer

FROM reg.vcoder.dev/webprovise/php:v2

COPY --from=composer /usr/bin/composer /usr/bin/composer

RUN composer global require hirak/prestissimo

# APP DIR
WORKDIR /var/www/html

COPY composer.json ./
COPY composer.lock ./

RUN composer install --prefer-dist --no-autoloader --no-scripts --no-progress && \
  composer clear-cache

COPY . /var/www/html

RUN chown -R www-data:www-data /var/www/html

RUN mkdir -p storage \
  && mkdir -p storage/framework/cache \
  && mkdir -p storage/framework/sessions \
  && mkdir -p storage/framework/testing \
  && mkdir -p storage/framework/views \
  && mkdir -p storage/logs \
  && chmod -R 777 storage \
  && composer dump-autoload --optimize --classmap-authoritative

EXPOSE 8080 

