<?php

return [
    'users' => [
        'change_email' => env('ALLOW_USER_CHANGE_EMAIL', false),
        'confirm_email' => env('USER_CONFIRM_EMAIL', false),
        'password_history' => env('SAVE_PASSWORD_HISTORY', true),
        'requires_approval' => env('USER_REQUIRED_APPROVAL', false),
    ],
    'roles' => [
        'super_admin_role' => 'super_admin',
        'admin_role' => 'admin',
    ],
    'guards' => [
        'web' => 'web',
        'api' => 'gateway',
    ]
];
